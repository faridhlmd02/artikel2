<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{

	public function index()
	{
		$data['judul'] = 'Tambah Artikel';

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isi', 'Isi', 'required');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar');
			$this->load->view('templates/topbar');
			$this->load->view('artikel/index');
			$this->load->view('templates/footer');
		} else {
			$judul = htmlspecialchars($this->input->post('judul', true));
			$isi = htmlspecialchars($this->input->post('isi', true));
			$penulis = htmlspecialchars($this->input->post('penulis', true));
			$tanggal  = htmlspecialchars($this->input->post('tanggal', true));
			$gambar = $_FILES['gambar'];

			if (empty($gambar)) { } else {
				$config['upload_path']		= './assets/img';
				$config['allowed_types']	= 'png|jpg|gif';
				$config['max_size']			= 1024;

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('gambar')) {
					echo "upload gagal";
					die();
				} else {
					$gambar = $this->upload->data('file_name');
				}

				$data = [
					'judul' 	=> $judul,
					'isi'		=> $isi,
					'penulis'	=> $penulis,
					'tanggal'	=> $tanggal,
					'gambar' 	=> $gambar
				];
				$this->db->insert('tb_artikel', $data);
			}
			redirect('home');
		}
	}
	public function delete($id)
	{
		$this->db->delete('tb_artikel', ['id' => $id]);
		echo '<script>Data ini telah dihapus</script>';
		redirect('home');
	}

	public function update($id)
	{
		$data['judul'] = 'Edit Artikel';
		$data['ubah_artikel'] = $this->db->get_where('tb_artikel', ['id' => $id])->row_array();
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isi', 'Isi', 'required');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar');
			$this->load->view('templates/topbar');
			$this->load->view('artikel/update', $data);
			$this->load->view('templates/footer');
		} else {
			$judul = $this->input->post('judul');
			$isi = $this->input->post('isi');
			$penulis = $this->input->post('penulis');
			$tanggal = $this->input->post('tanggal');

			$upload_gambar = $_FILES['gambar']['name'];
			if ($upload_gambar) {
				$config['allowed_types'] = 'jpg|png|gif';
				$config['max_size']		 = '1024';
				$config['upload_path']   = './assets/img/';

				$this->load->library('upload', $config);

				if ($this->upload->do_upload('gambar')) {
					$new_image = $this->upload->data('file_name');
					$this->db->set('gambar', $new_image);
				} else {
					echo $this->upload->display_errors();
				}
			}

			$this->db->set('judul', $judul);
			$this->db->set('isi', $isi);
			$this->db->set('penulis', $penulis);
			$this->db->set('tanggal', $tanggal);
			$this->db->where('id', $id);
			$this->db->update('tb_artikel');
			redirect('home');
		}
	}

	public function details($id)
	{
		$data['judul'] = 'Detail Artikel';
		$data['detail'] = $this->db->get_where('tb_artikel', ['id' => $id])->row_array();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('artikel/detail', $data);
		$this->load->view('templates/footer');
	}
}
