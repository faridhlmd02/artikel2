<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function index()
	{
		$data['judul'] = 'Artikel CodeIgniter';
		$data['artikel'] = $this->db->get('tb_artikel')->result_array();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/index', $data);
		$this->load->view('templates/footer');
	}

	public function delete($id)
	{
		$this->db->delete('tb_artikel', ['id' => $id]);
		echo '<script>Data ini telah dihapus</script>';
		redirect('home');
	}

	public function update($id)
	{
		$data['judul'] = 'Ubah Data Siswa';
		$data['ubah_artikel'] = $this->db->get_where('siswa', ['id' => $id])->row_array();

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isi', 'Isi', 'required');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar');
			$this->load->view('templates/topbar');
			$this->load->view('artikel/update');
			$this->load->view('templates/footer');
		} else {
			$data = [
				"judul" => $this->input->post('judul', true),
				"isi" => $this->input->post('isi', true),
				"penulis" => $this->input->post('penulis', true),
				"tanggal" => $this->input->post('tanggal', true),
				"gambar" => $this->input->post('gambar', true),
			];

			$this->db->where('id', $this->input->post('id'));
			$this->db->update('tb_artikel', $data);
			redirect('artikel');
		}
	}
}
